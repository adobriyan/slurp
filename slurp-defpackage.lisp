(defpackage "SLURP"
  (:use "CL")
  (:export "SLURP"
           "STREAM-LENGTH"))
