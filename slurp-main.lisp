(in-package "SLURP")

(defun stream-length (stream)
  (check-type stream stream)
  (handler-case
      (file-length stream)
    (type-error () nil)))

(defgeneric slurp (arg output-type-spec)
  (:method ((string string) output-type-spec)
    (slurp (pathname string) output-type-spec))

  (:method ((pathname pathname) output-type-spec)
    (with-open-file (stream pathname :element-type '(unsigned-byte 8))
      (slurp stream output-type-spec)))

  (:method ((stream stream) output-type-spec)
    (cond ((eq output-type-spec 'list)
           (let ((stream-element-type (stream-element-type stream)))
             (cond ((subtypep stream-element-type 'character)
                    (loop for c = (read-char-no-hang stream nil nil)
                          while c
                          collect c))

                   ((subtypep stream-element-type 'integer)
                    (loop for b = (read-byte stream nil nil)
                          while b
                          collect b))

                   (t
                    (error 'simple-type-error
                           :format-control "Unexpected stream element type ~S."
                           :format-arguments stream-element-type
                           :expected-type '(or character integer)
                           :datum stream-element-type)))))

          ((eq output-type-spec 'vector)
           (let ((stream-element-type (stream-element-type stream)))
             (cond ((subtypep stream-element-type 'character)
                    (with-output-to-string (string)
                      (loop for c = (read-char-no-hang stream nil nil)
                            while c
                            do (write-char c string))))

                   ((subtypep stream-element-type 'integer)
                    (let ((stream-length (stream-length stream)))
                      (if stream-length
                          (let* ((seq (make-array stream-length :element-type stream-element-type))
                                 (len-seq (read-sequence seq stream)))
                            (if (< len-seq stream-length)
                                (subseq seq 0 len-seq)
                                seq))
                          (let ((tmp (make-array 0 :element-type stream-element-type :adjustable t :fill-pointer 0)))
                            (loop for b = (read-byte stream nil nil)
                                  while b
                                  do (vector-push-extend b tmp))
                            (make-array (length tmp) :element-type stream-element-type :initial-contents tmp)))))

                   (t
                    (error 'simple-type-error
                           :format-control "Unexpected stream element type ~S."
                           :format-arguments stream-element-type
                           :expected-type '(or character integer)
                           :datum stream-element-type)))))

          (t
           (error 'simple-error
                  :format-control "Unexpected type specifier ~S."
                  :format-arguments (list output-type-spec))))))
